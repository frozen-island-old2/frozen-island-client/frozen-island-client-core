import SceneType from "./SceneTypes";
import Base from "./Base";
export default abstract class extends Base {
    constructor(gameElement: HTMLDivElement){
        super(gameElement);
    }
    protected __type(): SceneType {
        return SceneType.OfflineMenu;
    }
    public abstract setup(): void;
};
