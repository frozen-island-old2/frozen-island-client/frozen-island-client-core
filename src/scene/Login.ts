import OnlineMenu from "./OnlineMenu";
export default class extends OnlineMenu {
    private client: any;
    private userBox: HTMLInputElement | null;

    constructor(gameElement: HTMLDivElement, socket:SocketIOClient.Socket, Client: any){
      super(gameElement, socket);
      this.client = Client;
      this.userBox = null;
    }

    setup() {
        let {
            gameElement
        } = this;
        let scene = this.sceneElement(); {
            scene.style.fontSize = "1vw";
            scene.style.backgroundColor = "#dafbff";

            // The loading message.
            {
                let loadBox = document.createElement("div");
                loadBox.style.height = "15%";
                loadBox.style.width = "20%";
                loadBox.style.position = "absolute";
                loadBox.style.top = "50%";
                loadBox.style.left = "50%";
                loadBox.style.transform = "translate(-50%, -50%)";

                {
                    let userBox = document.createElement("input");
                    userBox.style.height = "35%";
                    userBox.style.width = "calc(100% - 0.25em*2 - 1em*2)";
                    userBox.style.backgroundColor = "white";
                    userBox.style.color = "black";
                    userBox.style.paddingLeft = "1em";
                    userBox.style.paddingRight = "1em";
                    userBox.style.border = "var(--border-color) solid 0.25em";
                    userBox.style.position = "relative";
                    userBox.style.overflow = "hidden";
                    userBox.style.cursor = "text";
                    userBox.style.borderRadius = "1em";
                    userBox.placeholder = "Username";
                    userBox.setAttribute("maxlength", "20");
                    this.userBox = userBox;
                    let self = this;
                    userBox.addEventListener('keypress', (e) => {
                        var key = e.which || e.keyCode;
                        if (key === 13) { // 13 is enter
                          self.login();
                        }
                    });
                    loadBox.appendChild(userBox);
                } {
                    let loginButton = document.createElement("div");
                    loginButton.style.marginTop = "10%";
                    loginButton.style.height = "auto";
                    loginButton.style.width = "calc(100% - 0.25em*2 - 1em*2)";
                    loginButton.style.color = "white";
                    loginButton.style.fontWeight = "bold";
                    loginButton.style.textAlign = "center";
                    loginButton.style.border = "var(--border-color) solid 0.25em";
                    loginButton.style.paddingLeft = "1em";
                    loginButton.style.paddingRight = "1em";
                    loginButton.style.backgroundColor = "var(--main-color)";
                    loginButton.style.borderRadius = "1em";
                    loginButton.style.cursor = "pointer";
                    loginButton.innerHTML = "Connect to Frozen Island";
                    loginButton.addEventListener("click", () => this.login());
                    loadBox.appendChild(loginButton);
                }

                scene.appendChild(loadBox);
            }
        }
        gameElement.innerHTML = '';
        gameElement.appendChild(scene);
    }

    login() {
        if(this.userBox !== null){
            let username = this.userBox.value.trim();
            if (username == "") {
                this.userBox.placeholder = "Please enter a username";
            } else {
                this.client.username = username;
                this.socket.emit("sign-in", username);
            }
        }
    }

    public spoke(_: any): void {
        console.error("Tried to  within the loading scene!");
    }

    public joined(_: any): void {
        console.error("Tried to join within the loading scene!");
    }
    public left(_: any): void {
        console.error("Tried to leave within the loading scene!");
    }
    public walked(_: any): void {
        console.error("Tried to walk within the loading scene!");
    }
    public throw(_: any): void {
        console.error("Tried to throw within the loading scene!");
    }

    public resize(): void {
        console.error("Tried to resize within the loading scene!");
    }
}
