import Base from "./Base";

interface ErrorData {
    ErrorMain: string;
    ErrorMessage: string;
    ErrorCode: number;
};

enum errorTypes {
    displayUnKillableError = "displayUnKillableError",
    displayError = "displayError"
};

export default class extends Base {
    private errorElement: HTMLDivElement;
    private errorBox: HTMLDivElement | null;
    private errorData: ErrorData | null;

    constructor(errorElement: HTMLDivElement, errorNumber: number, errorData: string[]) {
        super(errorElement);
        this.errorElement = this.gameElement;
        this.errorBox = null;
        this.errorData = null;
        this.errorContent(errorNumber, errorData);
    }

    displayError(): void {
        this.displayUnKillableError();
        if(this.errorBox !== null){
            let closeButton = document.createElement("div");
            closeButton.style.textAlign = "center";
            closeButton.style.border = "orange solid 0.25em";
            closeButton.style.display = "block";
            closeButton.style.width = "50%";
            closeButton.style.margin = "auto";
            closeButton.style.cursor = "pointer";
            closeButton.innerHTML = "close";
            closeButton.addEventListener("click", event => this.close(event));
            this.errorBox.appendChild(closeButton);
        }
    }

    displayUnKillableError() {
        if(this.errorData !== null)
        {
            let {
                ErrorMain,
                ErrorMessage,
                ErrorCode
            } = this.errorData;
            if(this.errorBox === null){
                let errorBox = document.createElement("div");
                errorBox.style.height = "20%";
                errorBox.style.width = "20%";
                errorBox.style.position = "absolute";
                errorBox.style.top = "50%";
                errorBox.style.left = "50%";
                errorBox.style.transform = "translate(-50%, -50%)";
                errorBox.style.background = "red";
                errorBox.style.border = "1em solid orange";
                errorBox.style.borderRadius = "1em";
                errorBox.style.padding = "0.5em";
                errorBox.style.fontSize = "1vw";
                errorBox.style.zIndex = "999";
                errorBox.innerHTML = `
            <h1 style="margin: 0;font-size: 1.5em;">${ErrorMain}</h1>
            <p style="margin-top: 0.25em;">${ErrorMessage}</p>
            <p style="position: absolute;bottom: 0.25em;right: 0.25em;margin: 0;">${ErrorCode}</p>
            `;
                this.errorBox = errorBox;
                this.errorElement.appendChild(errorBox);
            }
        }
    }

    errorContent(errorNumber: number, params: string[]): void{
      let type: errorTypes = errorTypes.displayUnKillableError;
      let title: string = "Unknown Error";
      let message: string = "There is no message for this error";
      switch(errorNumber){
        case 1:{
          title = "Disconnected";
          message = "Reason: " + params[0];
          break;
        }
        case 2:{
          title = "Username Taken";
          message = "That username is already in use, please try another.";
          type = errorTypes.displayError;
          break;
        }
        case 5:{
          title = "Username must be SFW"
          message = `The username "${params[0]}" contains profanity, please choose another.`;
          break;
        }
        case 6:{
          title = "You have been kicked."
          message = `You were kicked for saying "${params[0]}"`;
          break;
        }
        case 99:{
          title = "Invalid packet sent by client"
          message = `The server didn't like something we sent.`;
          break;
        }
      }
      this.errorData = {
            ErrorMain: title,
            ErrorMessage: message,
            ErrorCode: errorNumber,
      };
      this[type]();
    }

    close(event: Event): void {
        event.stopPropagation();
        if(this.errorBox !== null){
            this.errorBox.remove();
        }
    }
}
