enum SceneTypes {
    Base,
    OfflineMenu,
    BaseOnline,
    Room,
    RoomWithMinigame,
    OnlineMenu,
};
export default SceneTypes;
