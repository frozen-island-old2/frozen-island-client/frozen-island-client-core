// TODO: Get rid of this.
import SceneType from "./SceneTypes";
import BaseOnline from "./BaseOnline";

export default abstract class extends BaseOnline {
    protected __type(): SceneType {
        return SceneType.OnlineMenu;
    }
    public abstract setup(): void;
};
