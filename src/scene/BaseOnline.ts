import SceneType from "./SceneTypes";
import Base from "./Base";
export default abstract class extends Base {
    protected socket: SocketIOClient.Socket;

    constructor(gameElement: HTMLDivElement, socket: SocketIOClient.Socket) {
        super(gameElement);
        this.socket = socket;
    }

    protected __type(): SceneType {
        return SceneType.BaseOnline;
    }
}
