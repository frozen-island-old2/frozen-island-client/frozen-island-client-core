import SceneType from "./SceneTypes";
import Base from "./BaseOnline";

export default abstract class extends Base {
    protected __type(): SceneType {
        return SceneType.Room;
    }
    public abstract readonly roomName: string;

    public abstract setup(): void;
    public abstract preloadPlayers(data: any): void;

    public abstract spoke(data: any): void;

    public abstract joined(data: any): void
    public abstract left(data: any): void;
    public abstract walked(data: any): void;
    public abstract throw(data: any): void;

    public abstract resize(): void;
}
