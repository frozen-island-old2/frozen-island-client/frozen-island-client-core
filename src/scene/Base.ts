import SceneType from "./SceneTypes";
export default abstract class {
    public readonly type: SceneType;
    protected gameElement: HTMLDivElement;
    protected timeouts: Array<number>;
    protected intervals: Array<number>;

    constructor(gameElement: HTMLDivElement) {
        this.gameElement = gameElement;
        this.timeouts = [];
        this.intervals = [];
        this.type = this.__type();
    }

    protected __type(): SceneType {
        return SceneType.Base;
    }

    public sceneElement() {
        let sceneElement = document.createElement("div"); {
            sceneElement.style.position = "absolute";
            sceneElement.style.top = "0";
            sceneElement.style.left = "0";
            sceneElement.style.right = "0";
            sceneElement.style.bottom = "0";
        }
        return sceneElement;
    }

    public destroy() {
        let {
            timeouts,
            intervals
        } = this;
        for (let timeout of timeouts) {
            clearTimeout(timeout);
        }
        for (let interval of intervals) {
            clearInterval(interval);
        }
    }
}
