import SceneTypes from "./SceneTypes";
import Base from "./Base";
import OfflineMenu from "./OfflineMenu";
import BaseOnline from "./BaseOnline";
import Room from "./Room";
import OnlineMenu from "./OnlineMenu";

export function isOnline(scene: Base | OfflineMenu | BaseOnline | Room | OnlineMenu): scene is BaseOnline {
    switch(scene.type){
        case SceneTypes.BaseOnline:
        case SceneTypes.OnlineMenu:
        case SceneTypes.Room:
        case SceneTypes.RoomWithMinigame:
            return true;
        default:
            return false;
    }
}

export function isRoom(scene: Base | OfflineMenu | BaseOnline | Room | OnlineMenu): scene is Room {
    switch(scene.type){
        case SceneTypes.Room:
        case SceneTypes.RoomWithMinigame:
            return true;
        default:
            return false;
    }
}

export function isMenu(scene: Base | OfflineMenu | BaseOnline | Room | OnlineMenu): scene is OfflineMenu {
    switch(scene.type){
        case SceneTypes.OfflineMenu:
        case SceneTypes.OnlineMenu:
            return true;
        default:
            return false;
    }
}
