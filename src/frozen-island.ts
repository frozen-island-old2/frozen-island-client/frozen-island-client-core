// Importing the room types:
import Base from "./scene/Base";
import BaseOnline from "./scene/BaseOnline";
import OfflineMenu from "./scene/OfflineMenu";
import Room from "./scene/Room";
//And the constructor
import roomConstructorInterface from "./roomConstructorInterface";
// Exporting the base rooms
export {Base, BaseOnline, OfflineMenu, Room, roomConstructorInterface};


// Importing the specialty rooms:
// TODO: Make these belong in the roomConstructorInterface
import Loading from "./scene/Loading";
import Connecting from "./scene/Connecting";
import Login from "./scene/Login";
import ErrorBox from "./scene/Error";

import {isMenu} from "./scene/SceneTypeGuard";

export default class frozenIslandEngine{
    private gameElement: HTMLDivElement;
    private loadingElement: HTMLDivElement;
    private errorElement: HTMLDivElement;
    private socket: SocketIOClient.Socket;
    private loading: Loading;
    public disconnectReason: string[];
    public scene: Base;
    private username: string | null;
    private rooms: roomConstructorInterface;
    constructor(gameElement: HTMLDivElement, loadingElement: HTMLDivElement, errorElement: HTMLDivElement, rooms: roomConstructorInterface) {
        //new ErrorBox(errorElement, null, "Under Development", "Currently changeing a core feature. Game currently crashes.", "0").displayUnKillableError();
        this.gameElement = gameElement;
        this.loadingElement = loadingElement;
        this.errorElement = errorElement;
        this.rooms = rooms;
        this.socket = io({
            autoConnect: false,
            reconnection: false,
        });
        this.loading = new Loading(this.loadingElement);
        this.loading.setup();
        /*
        this.socket.on('moved-to', (...data) => this.changeRoom(data));
        this.socket.on('spoke', (...data) => this.scene.spoke(data));
        this.socket.on('joined', (...data) => this.scene.joined(data));
        this.socket.on('left', (...data) => this.scene.left(data));
        this.socket.on('walked', (...data) => this.scene.walked(data));
        this.socket.on('throw', (...data) => this.scene.throw(data));
        */
        this.disconnectReason = ["Lost Connection"];
        //window.addEventListener("resize", () => this.scene.resize());
        this.scene = new Connecting(this.gameElement);
        if(isMenu(this.scene)){
            // This is always going to be true, but it fixes type checking.
            this.scene.setup();
        }
        this.hideLoading();
        this.socket.connect();
        this.username = null;
    }

    hideLoading(): void {
        this.loadingElement.style.display = "none";
    }

    showLoading(): void {
        this.loadingElement.style.display = "block";
    }

    public connected(): void {
        this.showLoading();
        this.scene.destroy();
        this.scene = new Login(this.gameElement, this.socket, this);
        if(isMenu(this.scene)){
            this.scene.setup();
        }
        this.hideLoading();
    }

    public disconnected(): void {
      this.showLoading();
      this.error(1, this.disconnectReason);
      this.showLoading();
    }

    changeRoom(roomChangeData: [string, any, Object]) {
        let [room, players, state] = roomChangeData;
        this.showLoading();
        if(this.username == null){
            this.hideLoading();
            return;
        }
        let tempscene: Room | null = this.rooms.newRoom(room, this.gameElement, this.socket, this.username, state);
        if(tempscene !== null){
            tempscene.setup();
            tempscene.preloadPlayers(players);
            this.scene = tempscene;
        }
        this.hideLoading();
    }

    public error(ErrorNumber: number, Params: string[]) {
        new ErrorBox(this.errorElement, ErrorNumber, Params);
    }
}
