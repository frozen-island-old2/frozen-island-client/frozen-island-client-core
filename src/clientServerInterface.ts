/**
 * Client Server Interface.
 * That is the interface on the client side.
 */

import {default as frozenIslandEngine} from "./frozen-island";
import {isOnline, isRoom} from "./scene/SceneTypeGuard";

/**
 * The ClientServerInterface class
 *
 * Handles initial *incomeing* message verification,
 * ensureing that every message is *symanticly* valid.
 * @param socket The socket to the server.
 * @param game The game instance for refrence.
 */
export abstract class ClientServerInterface{
    /**
     * The socket to the server.
     */
    protected socket: SocketIOClient.Socket;
    /**
     * The game engine.
     * (Because we need to check the current scene,
     * and trigger events on the server.)
     */
    protected game: frozenIslandEngine;

    constructor(socket: SocketIOClient.Socket, game: frozenIslandEngine){
        this.socket = socket;
        this.game = game;
        this.builtinParser();
    }

    /**
     * Identifies if the scene is online or not.
     * @return true if scene has online methods.
     */
    protected isOnline(): boolean {
        return isOnline(this.game.scene);
    }

    /**
     * Identifies if the scene is a room or not.
     * @return true if scene is a room
     */
    protected isRoom(): boolean {
        return isRoom(this.game.scene);
    }

    /**
     * Returns the room name or null if scene is not a room.
     * @return room name or null
     */
    protected roomName(): string | null {
        if(isRoom(this.game.scene)){
            return this.game.scene.roomName;
        } else {
            return null;
        }
    }

    /**
     * Adds in builtin message handeling events.
     */
    private builtinParser(): void {
        this.socket.on("connect", ()=>this.connect());
        this.socket.on("issue", (...args:any[])=>this.issue(...args));
        this.socket.on("disconnectReason", (...args: any[])=>this.disconnectReason(...args));
        this.socket.on("disconnect", ()=>this.disconnect());
        this.connectParser();
    }

    /**
     * Custom game event handeling.
     */
    public abstract connectParser(): void;

    /**
     * `connect` event handeling.
     */
    protected connect(): void {
        this.game.connected();
    }

    /**
     * `issue` event handeling.
     * @param ...args the message paramaters.
     */
    protected issue(...args: any[]): void {
        if(args.length == 2){
            let [
                errorCode,
                errorParams
            ] = args;
            if((!isNaN(errorCode)) && Array.isArray(errorParams)){
                let isArrayOfStrings = function(array: Array<any>): array is Array<string> {
                    for(let item of array){
                        if(typeof item !== "string"){
                            return false;
                        }
                    }
                    return true;
                };
                if(isArrayOfStrings(errorParams)){
                    this.game.error(parseFloat(errorCode), errorParams);
                }
            }
        }
    }

    /**
     * `disconnectReason` event handeling.
     * @param ...args the message paramaters.
     */
    protected disconnectReason(...args: any[]): void {
        if(args.length >= 1){
            let [ reason ] = args[0];
            if(typeof reason == "string") {
                this.game.disconnectReason = [ reason ];
            }
        }
    }

    /**
     * `disconnect` event handeling.
     */
    protected disconnect(): void {
        this.game.disconnected();
    }

    /**
     * returns the parserVersion number. (Used for debugging.)
     * @return [description]
     */
    protected abstract parserVersion(): number;

}
