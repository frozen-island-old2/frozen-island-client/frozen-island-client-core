/// <reference types="socket.io-client" />
import Room from "./scene/Room";
export default abstract class {
    constructor();
    abstract newRoom(roomName: string, roomElement: HTMLDivElement, socket: SocketIOClient.Socket, username: string, roomState: Object): Room | null;
}
