/* globals io */
import Loading from "./scene/Loading";
import Connecting from "./scene/Connecting";
import Login from "./scene/Login";
import ErrorBox from "./scene/Error";
import { isMenu } from "./scene/SceneTypeGuard";
import io from "socket.io-client";
export default class {
    constructor(gameElement, loadingElement, errorElement, rooms) {
        //new ErrorBox(errorElement, null, "Under Development", "Currently changeing a core feature. Game currently crashes.", "0").displayUnKillableError();
        this.gameElement = gameElement;
        this.loadingElement = loadingElement;
        this.errorElement = errorElement;
        this.rooms = rooms;
        this.socket = io({
            autoConnect: false,
            reconnection: false,
        });
        this.loading = new Loading(this.loadingElement);
        this.loading.setup();
        /*
        this.socket.on('connect', () => this.connected());
        this.socket.on('issue', (...data) => this.error(data));
        this.socket.on('moved-to', (...data) => this.changeRoom(data));
        this.socket.on('spoke', (...data) => this.scene.spoke(data));
        this.socket.on('joined', (...data) => this.scene.joined(data));
        this.socket.on('left', (...data) => this.scene.left(data));
        this.socket.on('walked', (...data) => this.scene.walked(data));
        this.socket.on('throw', (...data) => this.scene.throw(data));
        this.socket.on('disconnectReason', (...reason) => this.disconnectReason = reason);
        
        this.disconnectReason = ["Lost Connection"];

        let self = this;
        this.socket.on('disconnect', (...data) => {
          self.showLoading();
          self.error([1, self.disconnectReason]);
          self.showLoading();
        });
        */
        //window.addEventListener("resize", () => this.scene.resize());
        this.scene = new Connecting(this.gameElement);
        if (isMenu(this.scene)) {
            // This is always going to be true, but it fixes type checking.
            this.scene.setup();
        }
        this.hideLoading();
        this.socket.connect();
        this.username = null;
    }
    hideLoading() {
        this.loadingElement.style.display = "none";
    }
    showLoading() {
        this.loadingElement.style.display = "block";
    }
    connected() {
        this.showLoading();
        this.scene.destroy();
        this.scene = new Login(this.gameElement, this.socket, this);
        if (isMenu(this.scene)) {
            this.scene.setup();
        }
        this.hideLoading();
    }
    changeRoom(roomChangeData) {
        let [room, players, state] = roomChangeData;
        this.showLoading();
        if (this.username == null) {
            this.hideLoading();
            return;
        }
        let tempscene = this.rooms.newRoom(room, this.gameElement, this.socket, this.username, state);
        if (tempscene !== null) {
            tempscene.setup();
            tempscene.preloadPlayers(players);
            this.scene = tempscene;
        }
        this.hideLoading();
    }
    error(data) {
        let [ErrorNumber, Params] = data;
        ErrorNumber = ErrorNumber + "";
        new ErrorBox(this.errorElement, ErrorNumber, Params);
    }
}
