import OfflineMenu from "./OfflineMenu";
export default class extends OfflineMenu {
    constructor(gameElement: HTMLDivElement);
    setup(): void;
}
