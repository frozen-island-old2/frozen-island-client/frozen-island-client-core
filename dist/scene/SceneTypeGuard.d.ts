import Base from "./Base";
import OfflineMenu from "./OfflineMenu";
import BaseOnline from "./BaseOnline";
import Room from "./Room";
import OnlineMenu from "./OnlineMenu";
export declare function isOnline(scene: Base | OfflineMenu | BaseOnline | Room | OnlineMenu): scene is BaseOnline;
export declare function isRoom(scene: Base | OfflineMenu | BaseOnline | Room | OnlineMenu): scene is Room;
export declare function isMenu(scene: Base | OfflineMenu | BaseOnline | Room | OnlineMenu): scene is OfflineMenu;
