import SceneType from "./SceneTypes";
import Base from "./Base";
export default class extends Base {
    constructor(gameElement, socket) {
        super(gameElement);
        this.socket = socket;
    }
    __type() {
        return SceneType.BaseOnline;
    }
}
