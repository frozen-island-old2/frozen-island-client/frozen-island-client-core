import OfflineMenu from "./OfflineMenu";
export default class extends OfflineMenu {
    constructor(gameElement) {
        super(gameElement);
    }
    setup() {
        let { gameElement } = this;
        let scene = this.sceneElement();
        {
            scene.style.backgroundColor = "#dafbff";
            // The loading message.
            {
                let loadBox = document.createElement("div");
                loadBox.style.height = "15%";
                loadBox.style.width = "20%";
                loadBox.style.position = "absolute";
                loadBox.style.top = "50%";
                loadBox.style.left = "50%";
                loadBox.style.transform = "translate(-50%, -50%)";
                // The loading bar
                {
                    let loadBar = document.createElement("div");
                    loadBar.style.height = "25%";
                    loadBar.style.width = "calc(100% - 0.25em*2)";
                    loadBar.style.backgroundImage = `
          repeating-linear-gradient(
                -45deg,
                var(--border-color),
                var(--border-color) 11px,
                white 10px,
                white 20px /* determines size */
              )`;
                    loadBar.style.backgroundSize = "28px 28px";
                    loadBar.style.border = "black solid 0.25em";
                    loadBar.style.position = "relative";
                    loadBar.style.overflow = "hidden";
                    {
                        let loadingKeyframes = [{
                                backgroundPosition: "0 0"
                            },
                            {
                                backgroundPosition: "28px 0"
                            }
                        ];
                        let loadingTimeing = {
                            duration: 3000,
                            iterations: Infinity
                        };
                        loadBar.animate(loadingKeyframes, loadingTimeing);
                    }
                    loadBox.appendChild(loadBar);
                }
                // The loading text
                {
                    let loadText = document.createElement("div");
                    loadText.style.marginTop = "10%";
                    loadText.style.height = "25%";
                    loadText.style.width = "100%";
                    loadText.style.color = "black";
                    loadText.style.textAlign = "center";
                    loadText.style.fontSize = "1vw";
                    loadText.innerHTML = "Loading";
                    loadBox.appendChild(loadText);
                }
                scene.appendChild(loadBox);
            }
        }
        gameElement.innerHTML = '';
        gameElement.appendChild(scene);
    }
}
