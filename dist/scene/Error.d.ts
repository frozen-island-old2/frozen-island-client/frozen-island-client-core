import Base from "./Base";
export default class extends Base {
    private errorElement;
    private errorBox;
    private errorData;
    constructor(errorElement: HTMLDivElement, errorNumber: number | string, errorData: Array<string>);
    displayError(): void;
    displayUnKillableError(): void;
    errorContent(errorNumber: string | number, params: Array<string>): void;
    close(event: Event): void;
}
