/// <reference types="socket.io-client" />
import OnlineMenu from "./OnlineMenu";
export default class extends OnlineMenu {
    private client;
    private userBox;
    constructor(gameElement: HTMLDivElement, socket: SocketIOClient.Socket, Client: any);
    setup(): void;
    login(): void;
    spoke(_: any): void;
    joined(_: any): void;
    left(_: any): void;
    walked(_: any): void;
    throw(_: any): void;
    resize(): void;
}
