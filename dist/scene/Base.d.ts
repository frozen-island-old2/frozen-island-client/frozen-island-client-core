import SceneType from "./SceneTypes";
export default abstract class {
    readonly type: SceneType;
    protected gameElement: HTMLDivElement;
    protected timeouts: Array<number>;
    protected intervals: Array<number>;
    constructor(gameElement: HTMLDivElement);
    protected __type(): SceneType;
    sceneElement(): HTMLDivElement;
    destroy(): void;
}
