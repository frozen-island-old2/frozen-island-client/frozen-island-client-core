import SceneType from "./SceneTypes";
import Base from "./BaseOnline";
export default abstract class extends Base {
    protected __type(): SceneType;
    abstract setup(): void;
    abstract preloadPlayers(data: any): void;
    abstract spoke(data: any): void;
    abstract joined(data: any): void;
    abstract left(data: any): void;
    abstract walked(data: any): void;
    abstract throw(data: any): void;
    abstract resize(): void;
}
