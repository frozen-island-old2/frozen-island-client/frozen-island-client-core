/// <reference types="socket.io-client" />
import SceneType from "./SceneTypes";
import Base from "./Base";
export default abstract class extends Base {
    protected socket: SocketIOClient.Socket;
    constructor(gameElement: HTMLDivElement, socket: SocketIOClient.Socket);
    protected __type(): SceneType;
}
