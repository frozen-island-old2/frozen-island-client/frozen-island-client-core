import SceneType from "./SceneTypes";
import Base from "./BaseOnline";
export default class extends Base {
    __type() {
        return SceneType.Room;
    }
}
