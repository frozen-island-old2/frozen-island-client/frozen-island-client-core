var SceneTypes;
(function (SceneTypes) {
    SceneTypes[SceneTypes["Base"] = 0] = "Base";
    SceneTypes[SceneTypes["OfflineMenu"] = 1] = "OfflineMenu";
    SceneTypes[SceneTypes["BaseOnline"] = 2] = "BaseOnline";
    SceneTypes[SceneTypes["Room"] = 3] = "Room";
    SceneTypes[SceneTypes["RoomWithMinigame"] = 4] = "RoomWithMinigame";
    SceneTypes[SceneTypes["OnlineMenu"] = 5] = "OnlineMenu";
})(SceneTypes || (SceneTypes = {}));
;
export default SceneTypes;
