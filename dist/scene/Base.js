import SceneType from "./SceneTypes";
export default class {
    constructor(gameElement) {
        this.gameElement = gameElement;
        this.timeouts = [];
        this.intervals = [];
        this.type = this.__type();
    }
    __type() {
        return SceneType.Base;
    }
    sceneElement() {
        let sceneElement = document.createElement("div");
        {
            sceneElement.style.position = "absolute";
            sceneElement.style.top = "0";
            sceneElement.style.left = "0";
            sceneElement.style.right = "0";
            sceneElement.style.bottom = "0";
        }
        return sceneElement;
    }
    destroy() {
        let { timeouts, intervals } = this;
        for (let timeout of timeouts) {
            clearTimeout(timeout);
        }
        for (let interval of intervals) {
            clearInterval(interval);
        }
    }
}
