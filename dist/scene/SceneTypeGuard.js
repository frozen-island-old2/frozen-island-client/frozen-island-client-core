import SceneTypes from "./SceneTypes";
export function isOnline(scene) {
    switch (scene.type) {
        case SceneTypes.BaseOnline:
        case SceneTypes.OnlineMenu:
        case SceneTypes.Room:
        case SceneTypes.RoomWithMinigame:
            return true;
        default:
            return false;
    }
}
export function isRoom(scene) {
    switch (scene.type) {
        case SceneTypes.Room:
        case SceneTypes.RoomWithMinigame:
            return true;
        default:
            return false;
    }
}
export function isMenu(scene) {
    switch (scene.type) {
        case SceneTypes.OfflineMenu:
        case SceneTypes.OnlineMenu:
            return true;
        default:
            return false;
    }
}
