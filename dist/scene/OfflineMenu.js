import SceneType from "./SceneTypes";
import Base from "./Base";
export default class extends Base {
    constructor(gameElement) {
        super(gameElement);
    }
    __type() {
        return SceneType.OfflineMenu;
    }
}
;
