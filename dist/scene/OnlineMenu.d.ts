import SceneType from "./SceneTypes";
import BaseOnline from "./BaseOnline";
export default abstract class extends BaseOnline {
    protected __type(): SceneType;
    abstract setup(): void;
}
