declare enum SceneTypes {
    Base = 0,
    OfflineMenu = 1,
    BaseOnline = 2,
    Room = 3,
    RoomWithMinigame = 4,
    OnlineMenu = 5
}
export default SceneTypes;
