import SceneType from "./SceneTypes";
import Base from "./Base";
export default abstract class extends Base {
    constructor(gameElement: HTMLDivElement);
    protected __type(): SceneType;
    abstract setup(): void;
}
