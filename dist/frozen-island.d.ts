import roomConstructor from "./roomConstructorInterface";
export default class {
    private gameElement;
    private loadingElement;
    private errorElement;
    private socket;
    private loading;
    private scene;
    private username;
    private rooms;
    constructor(gameElement: HTMLDivElement, loadingElement: HTMLDivElement, errorElement: HTMLDivElement, rooms: roomConstructor);
    hideLoading(): void;
    showLoading(): void;
    connected(): void;
    changeRoom(roomChangeData: [string, any, Object]): void;
    error(data: any): void;
}
